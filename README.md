# OpenERP8.0中国版本。

目标：降低OpenERP中国社区项目的参与门槛

面向群体和职责划分：

初级程序员 -- 将github上官方项目变更人工复制到本项目，并加中文变更记录

用户/顾问   -- 提交实施或日常使用过程中发现的issue

高级程序员 --  解决issue并将修改推送到github上的官方项目

汉化参与者 -- 持续更新汉化po文件
