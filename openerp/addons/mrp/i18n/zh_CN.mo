��    q     �  �  ,      �     �            	   0     :    F  u  J   �  �!  �  O#    H%  �  O(  p  >*  �  �,  �  `.  B   R0  B   �0     �0     �0  �   �0  %   }1  =   �1    �1     �2     3     3     +3     <3     B3  �   J3     �3     �3  1   �3     )4     14  �   ?4     �4     �4     5     *5     <5  d   X5     �5  <   �5  U   �5     T6     b6  	   p6     z6  )   �6     �6     �6  	   �6  5   �6  2   7  0   97  .   j7     �7     �7     �7     �7     �7      8     8  	   8     "8  
   *8     58     N8     c8     p8     ~8     �8     �8     �8     �8     �8     �8     �8     �8     9     9     :9     I9     X9     f9  
   z9  
   �9     �9     �9  .   �9  
   �9     �9  !   �9     :  Z   *:     �:     �:     �:     �:     �:     �:  V   �:  S   );     };     �;  	   �;     �;  n   �;     5<     E<  E   Y<  H   �<  ?   �<  )   (=     R=     [=  �   g=     �=     �=     >  K   	>  /   U>  i   �>  _   �>     O?     ]?     f?     y?     �?  �   �?     k@     }@     �@     �@     �@  3   �@  ;   �@     A     A     /A      KA     lA     tA     �A     �A     �A     �A     �A     �A  (   B     +B     @B  %   ^B      �B  7   �B  9   �B  9   C     QC     aC     zC     �C  "   �C     �C     �C     �C     �C     �C     �C     �C     
D  
   D  7   D     TD     ZD     kD  Z   {D  �   �D     aE  	   oE     yE     E  *   �E  
   �E     �E     �E     �E  	   �E     �E     �E  �   F     �F     �F     �F      �F     G     ,G     5G     AG     RG  5   ZG     �G     �G     �G     �G     �G     �G     �G     �G     H     #H     4H     EH     VH  _   bH     �H     �H     �H     �H     I      I     0I  
   9I     DI     XI     iI  &   �I  "   �I     �I     �I     �I     J     &J     >J     JJ     SJ     eJ     yJ     �J  
   �J     �J     �J     �J     �J     �J      K     	K  	   #K     -K     DK     JK  	   [K  %   eK  G   �K  ,   �K      L     	L  	   L     #L  )   /L     YL     aL  �   vL     3M  �   <M  
   &N     1N     @N     XN     hN     xN     �N     �N     �N     �N  "   �N     �N     �N  /   O     HO     XO     iO     rO  �   vO     �O     P      P  &   0P  %   WP  q   }P  
   �P     �P  
   Q     Q     Q     .Q  "   6Q  B   YQ  X   �Q  �   �Q  �   �R  �   �S  �   CT  �   �T     �U     �U     �U  "   �U     �U     V  U   <V  
   �V     �V     �V     �V  	   �V     �V     �V     �V     W     W  V   "W     yW     �W     �W     �W  
   �W     �W     �W     �W  7   �W  6   X     LX     TX     ]X  	   iX     sX     {X     �X     �X     �X     �X  �   �X  �   oY  K   Z  �  _Z     G\     S\     d\     v\     �\     �\     �\     �\     �\     �\  	   �\     ]  
   ]     #]  
   /]     :]  �   G]  2   �]     %^     *^     .^     2^     7^  �  ?^     �_     �_     �_     `     `  �   $`  9  �`  A  b  }  Pc  [  �d  Z  *g  �  �h  ;  j  k  �k  ?   'm  3   gm     �m     �m  x   �m  !   /n  *   Qn  b  |n     �o  	   �o     �o     p     "p     )p  o   0p     �p     �p  *   �p     �p     q  �   q     �q     �q     �q     �q     �q  7   �q     .r  2   ;r  Z   nr     �r     �r     �r     s     s     1s     8s  	   Es  (   Os  %   xs     �s     �s     �s     �s     �s     t     t     2t     Bt     It     Pt     Wt     ^t     st     �t     �t     �t     �t     �t     �t     �t     �t     �t     �t     �t     �t  !   u     &u     3u     Cu     Su  	   `u  	   ju     tu     �u  -   �u     �u     �u     �u     �u  E   v     Iv     Vv     cv     jv  	   wv     �v  H   �v  E   �v     w     $w  	   1w     ;w  H   Hw     �w     �w  *   �w  E   �w  *   x     Ax  	   `x  	   jx  s   tx     �x     �x     y  E   y  -   Ky  H   yy  E   �y  	   z     z     z     ,z     <z  0   Oz     �z     �z     �z     �z     �z     �z     �z     �z     �z     {  "   ${     G{     N{     U{     b{     r{     y{     �{  	   �{      �{  	   �{     �{     �{     �{     |  !   ,|     N|     m|     }|  	   �|     �|     �|     �|     �|     �|     �|  $   �|     �|  	   	}     }  	   }  $   $}     I}  	   P}  	   Z}  ?   d}  �   �}     %~     6~     C~     J~  %   Z~     �~     �~     �~  	   �~     �~     �~     �~  �   �~     y     �     �     �     �  	   �     �     �     �  $   �     �     )�     0�     C�     P�     ]�     j�     w�     ��     ��     ��     ��     ��  <   Ȁ     �     �     :�     M�     `�     |�  	   ��     ��     ��  	   ��     ��     ȁ     �     �     �     �     +�     8�     N�     U�     \�     l�     |�     ��     ��     ��     ��     ��  	   ǂ  	   т     ۂ     �  	   �     �  	   �     "�     /�  !   6�  *   X�     ��     ��     ��     ��  	   ��     ă     ��     �  �   �     ��  �        P�     f�     s�     ��     ��     ��     ��     ̅     Ӆ     څ     �     �     �  !   &�     H�     U�     h�     o�  ]   v�     Ԇ     �     �  $   ��  !    �  ]   B�     ��     ��     ��  	   ��     ˇ     ۇ     �  3   �  K   5�  �   ��  �   H�  K   ��  �   C�  �   Ȋ     ��     ��     ��     ̋     �     ��  B   �     N�     U�  	   g�  	   q�  	   {�     ��     ��     ��     ��     ��  %   ƌ     �     ��     �     �     �     !�     4�     D�  -   Q�  4   �     ��     ��          ύ     ֍  	   ݍ     �     �     �     �  Q   !�  �   s�  <   ��    9�     E�     R�     e�     x�     �     ��     ��     ��     ё     ޑ     �     ��     �     �     !�     .�  �   ;�  *   	�     4�     ;�     B�     I�     P�         !       �       �       R  �   m   �   Z   
   /   �   i  �   0  �   �   �                 P   u   Q      �   1             �       X      \    �   �   �   �   f      D      .      �   �           �   �   5       ]           �          2  �   w       l   �                   �   �   �   "  �      �                   �   A      E           �   K   �   1  �   #   _           8  |       �       2   @     �   Y      �   �   0   T  o  &   #     j   (   v   �   �   i   -       �          >   h   �   �   �   �       :       	  ^     �   l  7  x   �   C      �   +  G       L  ,   �   m  @   
  4   )               �       W  I     E        ;  O   >  9   &  �   �   �   �   z             <      b  �           '           �      3   U  H       �   �   %                 R   d  5      �   o   ,  <   �                    "           s                   ?           	   y   �   �           -  j  �   �   �   �       �   �   P  S         M      �   �   �   k     �       K  �               �   F   _  F  �       n       �   r         G  �       $  +   �   �       �   `  7     �   �   �   I          D   }      '  d       q   �   U   �   Q   B          �   V  J  �   /     X   a            �   B  �   �   �           =      �   a   :  �       �   �       �       Z                 �   �       )       �   c  {                4  �   p    W   p   J   �   S   N   �   �       e  ^   �   3          �   �       �   �       �   n  �                     ?  M   =             [   !          ;   �     �   �       �   ]  q      �        ~   e   [  *   c   V           h  �   .             6     �   T     f   �   t      O       �   9  �      L   `       \       �   �   �      Y           �   *     �   8       N  b   $   �       %   �       �   C  g   �           6      A       �   k          H  g      �                     (    Manufacture # Bill of Material # Manufacturing Orders %s (copy) %s produced 'Consume only' mode will only consume the products with the quantity selected.
'Consume & Produce' mode will consume as well as produce the products with the quantity selected and it will finish the production order when total ordered quantities are produced. <p class="oe_view_nocontent_create">
                Click to add a component to a bill of material.
              </p><p>
                Bills of materials components are components and by-products
                used to create master bills of materials.  Use this menu to
                search in which BoM a specific component is used.
              </p>
             <p class="oe_view_nocontent_create">
                Click to add a work center.
              </p><p>
                Work Centers allow you to create and manage manufacturing
                units. They consist of workers and/or machines, which are
                considered as units for task assignation as well as capacity
                and planning forecast.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a bill of material. 
              </p><p>
                Bills of Materials allow you to define the list of required raw
                materials used to make a finished product; through a manufacturing
                order or a pack of products.
              </p><p>
                Odoo uses these BoMs to automatically propose manufacturing
                orders according to procurement needs.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a group of properties.
              </p><p>
                Define specific property groups that can be assigned to your
                bill of materials and sales orders. Properties allows Odoo
                to automatically select the right bill of materials according
                to properties selected in the sales order by salesperson.
              </p><p>
                For instance, in the property group "Warranty", you an have
                two properties: 1 year warranty, 3 years warranty. Depending
                on the propoerties selected in the sales order, Odoo will
                schedule a production using the matching bill of materials.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a manufacturing order. 
              </p><p>
                A manufacturing order, based on a bill of materials, will
                consume raw materials and produce finished products.
              </p><p>
                Manufacturing orders are usually proposed automatically based
                on customer requirements or automated rules like the minimum
                stock rule.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a new property.
              </p><p>
                The Properties in Odoo are used to select the right bill of
                materials for manufacturing a product when you have different
                ways of building the same product.  You can assign several
                properties to each bill of materials.  When a salesperson
                creates a sales order, they can relate it to several properties
                and Odoo will automatically select the BoM to use according
                the needs.
              </p>
             <p class="oe_view_nocontent_create">
                Click to create a routing.
              </p><p>
                Routings allow you to create and manage the manufacturing
                operations that should be followed within your work centers in
                order to produce a product.  They are attached to bills of
                materials that will define the required raw materials.
              </p>
             <p class="oe_view_nocontent_create">
                Click to start a new manufacturing order. 
              </p><p>
                A manufacturing order, based on a bill of materials, will
                consume raw materials and produce finished products.
              </p><p>
                Manufacturing orders are usually proposed automatically based
                on customer requirements or automated rules like the minimum
                stock rule.
              </p>
             A factor of 0.9 means a loss of 10% during the production process. A factor of 0.9 means a loss of 10% within the production process. Active Active Id not found All product quantities must be greater than 0.
You should install the mrp_byproduct module if you want to manage extra products on BoMs ! Allow detailed planning of work order Allow several bill of materials per products using properties Allows to manage all product repairs.
* Add/remove products in the reparation
* Impact for stocks
* Invoicing (products and/or services)
* Warranty concept
* Repair quotation report
* Notes for the technician and for the final customer.
-This installs the module mrp_repair. Amount in cycles Amount in hours Amount measuring unit Analytic Journal Apply Approve Average delay in days to produce this product. In the case of multi-level BOM, the manufacturing lead times of the components will be added. Awaiting Raw Materials BOM Name BOM Product Variants needed form apply this line. BOM Ref BOM Structure Because the product %s requires it, you must assign a serial number to your raw material %s to proceed further in your production. Please use the 'Produce' button to do so. Bill Of Material Bill of Material Bill of Material Components Bill of Materials Bill of Materials Structure Bill of Materials allow you to define the list of required raw materials to make a finished product. BoM BoM "%s" contains a BoM line with a product recursion: "%s". BoM "%s" contains a phantom BoM line but the product "%s" don't have any BoM defined. BoM Hierarchy BoM Line Type BoM Lines BoM Type Can't find any generic Manufacture route. Cancel Cancel Production Cancelled Cannot consume a move with negative or zero quantity. Cannot delete a manufacturing order in state '%s'. Cannot find a bill of material for this product. Cannot find bill of material for this product. Capacity Information Capacity per Cycle Change Product Qty Change Quantity Change Quantity of Products Check Availability Code Companies Company Components Components Cost of %s %s Components suppliers Compute Data Configuration Configure Manufacturing Confirm Confirm Production Consume & Produce Consume Lines Consume Move Consume Only Consume Products Consumed Products Consumed for Cost Price per Unit of Measure Cost Structure Cost per cycle Cost per hour Costing Information Created by Created on Cycle Account Cycles Cost Date of the last message posted on the record. Day by day Default Unit of Measure Define manufacturers on products  Description Description of the Work Center. Explain here what's a cycle according to this Work Center. Destination Loc. Destination Location Done End Date Error! Extra Information Fill this only if you want automatic analytic accounting entries on production orders. Fill this product to easily track your production costs in the analytic accounting. Finished Products Finished Products Location Followers Force Reservation Forces to specify a Serial Number for all moves containing this product and generated by a Manufacturing Order General Account General Information Gives the sequence order when displaying a list of bills of material. Gives the sequence order when displaying a list of routing Work Centers. Gives the sequence order when displaying a list of work orders. Gives the sequence order when displaying. Group By Group By... Holds the Chatter summary (number of messages, ...). This summary is directly in html format in order to be inserted in kanban views. Hour Account Hourly Cost ID If a product variant is defined the BOM is available only for this product. If checked new messages require your attention. If the active field is set to False, it will allow you to hide the bills of material without removing it. If the active field is set to False, it will allow you to hide the routing without removing it. In Production Inactive Internal Reference Invalid Action! Is a Follower Keep empty if you produce at the location where the finished products are needed.Set a location if you produce at a fixed location. This can be a partner location if you subcontract the manufacturing operations. Last Message Date Last Updated by Last Updated on Late Location Location where the system will look for components. Location where the system will stock the finished products. Lot Manage Routings Manage repairs of products  Manage routings and work orders  Manager Manufacture Manufacture Rule Manufacture in this Warehouse Manufacturing Manufacturing Efficiency Manufacturing Lead Time Manufacturing Order Manufacturing Order <em>%s</em> created. Manufacturing Orders Manufacturing Orders To Start Manufacturing Orders Waiting Products Manufacturing Orders in Progress Manufacturing Orders which are currently in production. Manufacturing Orders which are ready to start production. Manufacturing Orders which are waiting for raw materials. Mark as Started Master Bill of Materials Master Data Messages Messages and communication history Mode Mrp Workcenter Name New No BoM exists for this product! No. Of Cycles No. Of Hours Normal Not urgent Not used in computations, for information purpose only. Notes Number of Cycles Number of Hours Number of iterations this work center has to do in the specified operation of the routing. Number of operations this Work Center can do in parallel. If this Work Center represents a team of 5 workers, the capacity per cycle is 5. Open MRP Menu Operation Order Order Planning Order quantity cannot be negative or zero! Parent BoM Parent Routing Partner Ref: Pending Per month Per week Phantom Phantom: this product line will not appear in the raw materials of manufacturing orders,it will be directly replaced by the raw materials of its own BoM, without triggeringan extra manufacturing order. Planning Please provide proper quantity. Print Print Cost Structure of Product. Printing date: Priority Procurement Procurement Rule Produce Produce several products from one manufacturing order Produced Products Product Product Cost Structure Product Move Product Name Product Price Product Produce Product Produce Consume lines Product Qty Product Quantity Product Rounding Product Template Product UOS Product UOS (Unit of Sale) is the unit of measurement for the invoicing and promotion of stock. Product UOS Qty Product UOS Quantity Product Unit of Measure Product UoS Product UoS Quantity Product Variant Product: Production Production Location Production Order Production Order N° : Production Order for Produced Products Production Order for Raw Materials Production Scheduled Product Production Started Production Work Centers Production progress Production started late Productions Products Products Consumed Products to Consume Products to Finish Products to Produce Properties Properties categories Properties composition Property Property Group Property Groups Quantity Quantity (in default UoM) Quantity: Raw Materials Location Ready Ready to Produce Reference Reference must be unique per Company! Reference of the document that generated this production order request. Reference to a position in an external plan. Resource Resource Leaves Resources Responsible Rounding applied on the product quantity. Routing Routing Work Centers Routing indicates all the Work Centers used, for how long and/or cycles.If Routing is indicated then,the third tab of a production order (Work Centers) will be automatically pre-completed. Routings Routings allow you to create and manage the manufacturing operations that should be followed within your work centers in order to produce a product. They are attached to bills of materials that will define the required raw materials. SO Number: Scheduled Date Scheduled Date by Month Scheduled Date: Scheduled Month Scheduled Products Scheduled goods Scrap Products Search Search Bill Of Material Search Bill Of Material Components Search Production Search for mrp workcenter Security days for each manufacturing operation. Select Quantity Select time unit Sequence Set Set: When processing a sales order for this product, the delivery order will contain the raw materials, instead of the finished product. Source Document Source Document: Source Location Specify Cost of Work Center per cycle. Specify Cost of Work Center per hour. Specify quantity of products to produce or buy. Report of Cost structure will be displayed base on this quantity. Start Date Status Stock Move Stock value Stock value variation Summary Supplier Price per Unit of Measure Technical field used to make the traceability of produced products The Product Unit of Measure you chose has a different category than in the product form. The list of operations (list of work centers) to produce the finished product. The routing is mainly used to compute work center costs during operations and to plan future loads on work centers based on production plannification. The list of operations (list of work centers) to produce the finished product. The routing is mainly used to compute work center costs during operations and to plan future loads on work centers based on production planning. The selection of the right Bill of Material to use will depend on the properties specified on the sales order and the Bill of Material. This allows to add state, date_start,date_stop in production order operation lines (in the "Work Centers" tab).
-This installs the module mrp_operations. This allows you to define the following for a product:
* Manufacturer
* Manufacturer Product Name
* Manufacturer Product Code
* Product Attributes.
-This installs the module product_manufacturer. Time after prod. Time before prod. Time for 1 cycle (hour) Time in hours for doing one cycle. Time in hours for the cleaning. Time in hours for the setup. Time in hours for this Work Center to achieve the operation of the specified routing. To Consume Total Cost of %s %s Total Cycles Total Hours Total Qty Track Manufacturing Lots Track production Type Type of period Unit of Measure Unit of Measure (Unit of Measure) is the unit of measurement for the inventory control Unread Messages Update Urgent User Valid From Valid From Date by Month Valid From Month Valid Until Validity of component. Keep empty if it's always valid. Validity of this BoM. Keep empty if it's always valid. Variant Variants Very Urgent Warehouse Warning Warning! Website Messages Website communication history Week Weekly Stock Value Variation Weekly Stock Value Variation enables you to track the stock value evolution linked to manufacturing activities, receipts of products and delivery orders. When processing a sales order for this product, the delivery order
                                will contain the raw materials, instead of the finished product. When products are manufactured, they can be manufactured in this warehouse. When the production order is created the status is set to 'Draft'.
                If the order is confirmed the status is set to 'Waiting Goods'.
                If any exceptions are there, the status is set to 'Picking Exception'.
                If the stock is available then the status is set to 'Ready to Produce'.
                When the production gets started then the status is set to 'In Production'.
                When the production is over, the status is set to 'Done'. Work Center Work Center Load Work Center Loads Work Center Operations Work Center Product Work Center Usage Work Center load Work Center name Work Centers Work Centers Utilisation Work Cost Work Cost of %s %s Work Order Work Orders WorkCenter Working Time You can configure by-products in the bill of material.
Without this module: A + B + C -> D.
With this module: A + B + C -> D + E.
-This installs the module mrp_byproduct. You must assign a serial number for the product %s days max min plus unknown Project-Id-Version: openobject-addons
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-09-23 16:27+0000
PO-Revision-Date: 2014-09-30 12:39+0800
Last-Translator: 保定-粉刷匠 <992102498@qq.com>
Language-Team: Chinese (Simplified) <zh_CN@li.org>
Language: zh_CN
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2013-03-08 05:50+0000
X-Generator: Poedit 1.6.9
 生产 # 物料清单 # 生产单 %s (副本) %s 已生产 '投料' 模式会仅消耗对应成品数量的原材料而无产成品产出。
'报工' 模式会消耗对应成品数量的原材料而且产出对应数量的产成品。 <p class="oe_view_nocontent_create">
                单击来在BOM中新建一个部件。
              </p><p>
                BOM部件是一个用来创建主BOM的部件或者副产品。
              使用这个菜单来搜索一个特定的部件被哪个BOM使用了。
              </p>
             <p class="oe_view_nocontent_create">
               单击来新建一个工作中心。
              </p><p>
                工作中心允许你建立和管理生产单元。它们由工人和（或）机
                 器组成，用于分配任务和进行能力、计划预测。
              </p>
             <p class="oe_view_nocontent_create">
                单击来新建BOM。
              </p><p>
                BOM允许你定义生产产品需要的原材料清单；
               产品生产通过生产单或产品组合来实现。
              </p><p>
                Odoo使用这些BOM，依据补货需求自动给出生产单建议。
              </p>
             <p class="oe_view_nocontent_create">
                点此来新建一个属性组。
              </p><p>
              定义特定的属性组可以分配给你的BOM和销售订单。
              属性允许Odoo自动根据销售人员的销售订单
              中的属性选择来匹配正确的BOM。
              </p><p>
              例如，在属性组“保修”，
              你有两个特性可以选择：1年保修，3年保修。
              根据销售订单中选择的属性，
              Odoo将使用匹配的材料清单来安排生产。
              </p> <p class="oe_view_nocontent_create">
                单击来新建一个生产单。
              </p><p>
                生产单基于BOM，将消耗原材料并生成成品。
              </p><p>
                生产单通常基于客户的需求或类似最小库存规则这样的规则自动产生。
              </p>
             <p class="oe_view_nocontent_create">
              点此创建新的属性。
              </p><p>
              当你有不同的方式来生产同样的产品时，
              Odoo的“属性”项将会被用来选择合适的BOM来制造产品。
              您可以对每个物料清单指定多个属性。
              当一个销售人员创建销售订单时，
              他们可以关联多个属性，Odoo的会自动选择需要使用的BOM。
              </p>             <p class="oe_view_nocontent_create">
               单击来新建工艺路线。
              </p><p>
                工艺路线允许你建立和管理为生产产品而在工作中心进行的生产操作。 
                它们附加在定义了所需原材料的BOM中。
              </p>
             <p class="oe_view_nocontent_create">
                单击开始一个新的生产单。 
              </p><p>
                生产单，基于BOM，消耗原料并生产完工产品.
              </p><p>
               生产单通常是 基于 客需求或者 类似最小库存规则这样的自动规则 自动产生的。
              </p>
             一个为0.9的因子意味着在生产流程中将损失10%。 指标值为0.9意味着生产过程中损耗了10% 生效 Active Id 找不到 所有的产品数量必须大于0。
如果你要在BOM上面管理额外的产品，你要安装mrp_byproduct模块。 运行工作中心的详细计划 使用属性，每个产品允许几个BOM 管理产品维修相关。
                    * 添加/删除要修复的产品
                    * 更改相应库存
                    * 开票（产品和/或服务）
                    * 保修设定 
                    * 维修报价单 
                    * 对技术人员和最终客户的说明.
             需安装模块mrp_repair. 循环次数 工时数 运行时间表示方式 辅助核算分录 应用 核准 生产这个产品的平均延期数。在多级BOM的情况下，组件的制造提前期会被加入进去。 等待原材料中…… 物料表名称 为应用这行需要的Bom多属性产品 物料表编号 物料清单结构 由于产品 %s 的需要，你必须为原材料 %s 指定一个序列号，在生产的后续过程中使用。请点击 ‘生产’ 按钮。 物料清单 物料清单 物料清单组件 物料清单 物料清单明细 BOM 允许你定义生产成品所需原料的列表。 物料清单 BoM "%s" 包含了一个递归的产品： "%s"。 BoM "%s" 包含了一个虚拟的行，但是这个产品 "%s" 没有被任何BoM定义。 物料表层次 物料清单行类型 物料清单明细 物料清单类型 不能找到生产路线。 取消 取消生产 已取消 数量为负数或者0的不能投料。 不能删除'%s'状态的生产单。 找不到这个产品的BOM 不能找到这个产品的BOM 能力信息 每循环生产能力 改变产品数量 修改数量 修改产成品数量 检查可用性 代码 公司 公司 部件 部件成本：%s %s 部件供应商 计算数据 设置 配置生产 确定 确认生产 报工 投料 报工 投料 消耗的产品 已投料数量 已投料数量 每一计量单位的成本价格 成本结构 每循环成本 每小时成本 成本信息 创建人 创建在 循环科目 按循环计算的成本 最新一个消息的发布日期的记录。 按天 默认计量单位 定义产品的生产商  说明 工作中心的描述。在这里输入这个工作中心的简介。 成品库位 目的库位 完成 结束日期 错误！ 其它信息 如果你要自动在生成订单进行辅助核算，只需填入这里 填入产品将更容易在辅助核算中跟踪你的生产成本。 成品 成品库位 关注者 强制预留 为包含这个产品的所有移动和生产单指定一个序列号。 总账 一般信息 输入序号用于物料清单列表排序 指定一个编号，用于在显示一系列工作中心时排序。 输入序号用于显示工作中心列表 显示时提供序列顺序。 分组按 分组... 保留复杂的摘要(消息数量,……等)。为了插入到看板视图，这一摘要直接是是HTML格式。 时间科目 小时成本 ID 如果定义了产品属性，则这个Bom仅适用于这个产品。 如果要求你关注新消息，勾选此项 如果去掉“启用”的勾，可以不用删除就隐藏物料清单 如果去掉这个启用的勾，工艺不需要删除就可以隐藏 生产中 未活动的 内部编号 非法的动作 是一个关注者 如果你在本地生产这成品你需要留空 最新消息日期 最后更新被 最后更新在 延迟 库位 部件查找库位。 系统存储成品的库位 批次 管理工艺路线 关联产品的修理  管理工艺路线和工作中心  经理 生产 生产规则 从生产补货 生产 生产效率 生产提前期 生产单 生产单 <em>%s</em>  创建。 生产单 未开始的生产单 等待原材料的生产单 未完成的生产单 已投产的制造订单 可以开始生产的制造订单 等待原材料的制造订单 标记为开始 主物料表 主数据 消息 消息和通信历史 模式 MRP 工作中心 名称 新建 该产品尚未定义物料清单！ 循环次数 工时数 普通 不紧急 不用于计算，仅作为信息。 备注 周期数 工时数 工作中心按照某种工艺路线操作所需执行的次数 此工作中心可以并行操作的数量。如果这个工作中心是5个工人的工作组，那么每循环的能力是5。 打开MRP 菜单 操作说明 订单 生产单编制 订单数量不能是负数或者0！ 上级物料清单 上级工艺路线 业务伙伴参考： 等待中 每月 每周 虚项 虚拟：这个产品行不会出现在生产订单的原材料中，它将用其自己BoM中的原材料替换它，这样不用产生另外的生产单。  计划 请提供合适的数量。 打印 打印产品成本结构 打印日期： 优先级 补货 补货规则 生产 从一个生产单生产几个产品 已生产的产品 产品 产品成本构成 产品移动 产品名称 产品价格 产品生产 未投料数量 数量 产品数量 产品舍入 产品模板 产品销售单位 销售单位是用于开发票和推销产品的计量单位 产品销售单位数量 产品的销售单位数量 产品计量单位 产品销售单位 产品的销售单位数量 产品属性 产品： 生产单编码 产品库位 生产单 生产单号: 已生产产品的生产单 生产单号: 计划生产的产品 生产已开始 生产工作中心 生产过程 未准时开始生产 生产 产品 已投料数量 未投料数量 未完工数量 要生产的成品 属性 属性分类 属性分类 属性 属性组 属性组 数量 数量（默认计量单位） 数量： 原料库位 准备好 准备生产 参考 编号必须在公司内唯一！ 生成这张生产单的前置单据编号 外部计划位置的参考 资源 员工请假 资源 负责人 生产数量的小数位数 工艺路线 工序所在工作中心 工艺表示要用到的所有工作中心，需要多少时间或循环次数。如果这里输入了工艺，制造订单的第三个选项卡（工艺）会自动填充。 工艺路线 通过工艺路线来创建并管理在工作中心排产时所需遵循的制造工序，并通过相应的BOM表确认所需的原材料。 销售订单编号： 下单日期 按月排定日期 计划日期： 预定月份 已纳入计划的产品 安排的货物 废料 搜索 搜索物料清单 搜索物料清单组件 搜索产品 工作中心列表 每个生产活动的确保天数 选择数量 选择时间单位 序列 套件 设置：在处理该产品的销售订单时，发货单将为原材料，而不是成品。 关联单据 源单据： 来源库位 输入每循环的工作中心成本 输入工作中心的工时成本 输入要生产或采购的产品数量。报表会列出这些数量该产品的成本结构 开始日期 状态 库存调拨 库存值 库存值变化 摘要 每一单位的供应商价格 打开已生产产品可追朔性的技术字段。 你选择的计量单位跟产品表单的计量单位在不同的类别。 制造产成品需要的工序列表（也就是工作中心的列表）。工艺主要用于计算工作中心在制造过程中的成本和在生产计划过程中规划工作中心的工作负载 生产产成品需要的一系列操作（发生在一系列工作中心上）。工艺主要用于计算工作中心成本和根据生产计划来规划工作中心负载。 选择使用正确的BOM将取决于销售订单和物料清单的属性。 在生产指令操作行中增加状态，开始日期，结束日期 （在"工作中心"标头）-需安装模块mrp_operations。 定义产品的下列属性：
               *生产商
               *生产商品名
               *生产商代码
               *产品特性
需安装模块“product_manufacturer”。 生产后时间 生产准备时间 一个周期的时间(小时) 一个周期的工作小时数 清理小时数 准备小时数 要完成特定工序在此工作中心上需要花费的小时数 投料 总成本：%s %s 总周期 总工时 总数量 跟踪生产批次 跟踪生产 类型 周期类型 计量单位 计量单位是存货的 计量单位 未读信息 （更新） 紧急 用户 有效期从 逐月确定日期 有效期月份 有效期到 组件的有效性。留空为总是有效。 物料清单的有效性，.留空为总是有效。 属性 属性 非常紧急 仓库 警告 警告！ 网站消息 网站交流历史 周 每周库存值变动 库存周报能帮您监控由生产、入库、出库引起的库存值的变化 在处理该产品的销售订单时，交货订单将包含原料，
                                                而不是成品。 当产品被制造时，他们可以在这个仓库生产。 当 生产单被创建时，状态是“草稿”。
                                   如果被确认，状态设置为“等待原料”
                                   如果有任何意外发生，状态被设置为“领料异常”
                                   如果库存可用了，状态被设置为“准备生产”。
                                   当生产开始的时候，状态被设置为“生产中”
                                   当生产完成后，状态被设置为“结束” 工作中心 工作中心负载 工作中心负载 工艺 工作中心产品 工作中心使用情况 工作中心负载 工作中心名称 工作中心 工作中心效用 工作成本 工时成本：%s %s 工单 工单 工作中心 工作时间 你可以在BOM里配置副产品。
                未安装该模块时: A + B + C -> D。
                已安装该模块时: A + B + C -> D + E。 
                -需安装模块 mrp_byproduct。 你必须为这个产品制定序列号 %s 天数 最大 最小 添加 未知 